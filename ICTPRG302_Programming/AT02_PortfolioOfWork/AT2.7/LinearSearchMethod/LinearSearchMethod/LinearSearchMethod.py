def search(list,target): #Defines parameter(s) for search
  
    for i in range(len(list)): #loops through the list and keeps track of the length
        if list[i] == target: #Checks if "i" is the same as target
            index = list.index(target) #Finds the index of the target within the list
            return index 
    return -1 
   
  

list = ["mango", "watermelon", "apple", "orange", "grape", "banana"] 
  
target = input("Please input here:")
returnSearch = search(list, target) #returnMessage = list and target input

if (returnSearch != -1): #if returnSearch does not equal -1 it will run
    print("Found:", target, returnSearch) 
elif returnSearch == -1: #Will run if returnSearch == -1
    print(target + ":","Was Not Found") 

