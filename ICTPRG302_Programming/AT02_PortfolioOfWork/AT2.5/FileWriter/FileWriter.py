file = open("news.txt", "w") #Opens a file in writing mode called "news.txt" 
file.writelines("According to a report by PC Gamer, the recent Twitch hack revealed \
that the platform's most massive payouts were only available to the top 0.01% of streamers.") #File.writelines allows the text to be printed into the "news.txt" file.
# "\" Breaks up the code into 2 lines, but still reads it as 1 line.
file.close #closes the txt file
