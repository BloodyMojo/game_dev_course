Temp = input("Please input body temperature:") #Allows the user to input their temperature.
Temp = int(Temp) #Converts the string into an integer.

if(Temp in range (37, 38)): #Check to see if your body temp is between this range.
    print("Normal Body temperature") #Prints t

elif(Temp in range (38, 39)): #Since 38 gets updated again it will use this elif statement instead of the previous if statement.
    print("Is a Fever")

elif(Temp in range (39, 40)):
    print("Is a High Fever ")

elif(Temp in range (40, 41)):
    print("Is A Very High Fever")

elif(Temp >= 41): #checks anything thats is 41 or above.
    print("Is A Serious Emergency")
