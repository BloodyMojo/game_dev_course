def search(tuple, newTarget): #Defines parameter(s) for search

    for i in range(len(tuple)): #loops through the tuple and keeps track of length
        if tuple[i] == newTarget: #Checks if newTarget is within tuple
           index = tuple.index(newTarget) #Finds index of target within tuple
           return index
    return -1

tuple = ("Windows 10", "Linux Mint", "Mac OS 11", "Android Oreo", "Android Pie", "Android 11", "iOS 14")
tupleClass = type(tuple) #Finds what class tuple is

target = input("Plese input here:") 
newTarget = target.title() #Capitalizes every new world of the input
newTarget = newTarget.replace("Ios 14", "iOS 14") #Convets the capitalized version of iOS 14 into correct spelling
newTarget = newTarget.replace("Mac Os 11", "Mac OS 11") #Convets the capitalized version of Mac OS 11 into correct spelling

returnSearch = search(tuple, newTarget) #returnSeach = tuple and newtarget string

if (returnSearch != -1): #Sees if returnSearch does not = -1, if it doesn't it continues 
    print("Found:", newTarget)
    print("Target Index:", returnSearch)
elif returnSearch == -1: #Sees if returnSearch = -1
    print(newTarget + ":", "Was Not Found")

print("You are searching:", tupleClass) #Prints string + tupleClass