output_string = "Python has been an important part of Google since the beginning, and remains so as the system grows and evolves." +\
"\nToday dozens of Google engineers use Python, and we're looking for more people with skills in this language. said Peter Norvig, director of search quality at Google, Inc."
#Output_string is a string with the text inside. \n makes the text a new line. \allows the text to be seperated in python.


file = open("Python.txt", "w") #Opens a file called Python.txt that you can write in
file.writelines(output_string)  #Write output_string in Python.txt
file.close #Closes the file

file = open("Python.txt") #Opens Python.txt file that is only readable
input_string = file.readlines() #Reads each line that is in Python.txt
for line in input_string: #Prints each line within input_string
    print(line)

file.close #closes file
